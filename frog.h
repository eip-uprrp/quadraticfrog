#ifndef FROG_H
#define FROG_H

#include <QWidget>
#include <QPainter>
#include <QtGui>
#include <QtCore>
#include <QTimer>
#include <QLabel>


/// Class that represent the frog and leaves.
///
/// Frog is a subclass of QWidget.

class frog : public QWidget
{
    Q_OBJECT
public:

    /// \fn frog::frog(QWidget *parent)
    /// \~English
    /// \brief Default constructor.
    /// * Ysapo: Y coordinate of the frog, set at 190 so it sits on top of the leaf
    /// * Xsapo: X coordinate of the frog, a random number from 0 to -30
    /// * xnenu, xnenu2, xnenu3: X coordinates of the leafs
    /// * textBool: Boolean for the labels on the leafs
    /// * temp, temp2: X coordinate of the background
    /// \~Spanish
    /// \ brief Constructor por defecto.
    /// * Ysapo: Coordenada Y del sapo, inicializado a 190 para que aparesca encima del nenufar
    /// * Xsapo: Coordenada X del sapo, un numero aleatorio de 0 a -30
    /// * xnenu, xnenu2, xnenu3: coordenadas x de las hojas
    /// * textBool: Booleana para las etiquetas de los nenufares
    /// * temp, temp2: Coordenada X del fondo
    ///
    explicit frog(QWidget *parent = 0);

    /// \fn frog::QuadraticPlus(int a, int b, int c)
    /// \~English
    /// \brief Function that returns the result of the positive quadratic formula.
    /// \param a: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param b: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param c: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \~Spanish
    /// \brief Funcion que devuelve el resultado de la formula cuadratica positiva.
    /// \param a: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param b: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param c: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    ///
    float QuadraticPlus(int a, int b, int c);

    /// \fn frog::QuadraticMinus(int a, int b, int c)
    /// \~English
    /// \brief Function that returns the result of the negative quadratic formula.
    /// \param a: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param b: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param c: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \~Spanish
    /// \brief Funcion que devuelve el resultado de la formula cuadratica negativa.
    /// \param a: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param b: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
    /// \param c: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
    ///
    float QuadraticMinus(int a, int b, int c);

    /// \fun frog::~frog()
    /// \~English
    /// \brief Default destructor.
    /// \~Spanish
    /// \brief Destructor por defecto.
    ///
    ~frog();


    float Ysapo; /**< Y coordinate of the frog*/
    float Xsapo; /**< X coordinate of the frog*/
    float initialX; /**< Initial position of the frog*/
    bool flag, xFlag, backFlag, moved, textBool;
    int A, B, C, buttonCounter,temp,temp2,xnenu,xnenu2,xnenu3;


signals:

public slots:
    /// \fun void frog::mySlot()
    /// \~English
    /// \brief Function connected to a timer, used to simulate the
    ///  frog's jump.
    /// \~Spanish
    /// \brief Funcion conectada a un reloj, utilizada para simular
    ///  el brinco del sapo
    ///
    void mySlot();

    /// \fn void frog::run(int , int , int )
    /// \~English
    /// \brief Function that is invoked when the jump button is
    ///  pressed, makes the frog jump to the coordinates calculated
    ///  with the a, b, c values provided by the user.
    /// \param aa value provided by user to complete the quadratic equation ax^2 + bx + c
    /// \param bb value provided by user to complete the quadratic equation ax^2 + bx + c
    /// \param cc value provided by user to complete the quadratic equation ax^2 + bx + c
    /// \~Spanish
    /// \brief Funcion que es invocada cuando el boton de brincar es
    ///  presionado, hace que el sapo salte a las coordenadas calculadas
    ///  con los valores de a, b, c proporcionados por el usuario.
    /// \param aa valor proporcionado por el usuario utilizado para completar la ecuacion
    ///  cuadratica ax^2 + bx + c
    /// \param bb valor proporcionado por el usuario utilizado para completar la ecuacion
    ///  cuadratica ax^2 + bx + c
    /// \param cc valor proporcionado por el usuario utilizado para completar la ecuacion
    /// cuadratica ax^2 + bx + c
    void run(int, int, int);

    /// \fn void frog::reset()
    /// \~English
    /// \brief Function that is invoked when the retry button is clicked, resets
    ///  the frog and the leafs to a random starting position.
    /// \~Spanish
    /// \brief Funcion que es invocada cuando el boton de "retry" es presionado,
    ///  restablece el sapo y los nenufares a una posicion de partida aleatoria.
    ///
    void reset();

    /// \fn void frog::updatenenu()
    /// \~English
    /// \brief Function that updates the position of the frog and the leafs
    ///  when the frog reaches the end of the screen when it's sliding back.
    /// \~Spanish
    /// \brief Funcion que actualiza la posicion del sapo y los nenufares
    ///  cuando el sapo llega al final de la pantalla cuando se desliza hacia atras.
    ///
    void updatenenu();

protected:
    /// \fn void frog::paintEvent(QPaintEvent *event)
    /// \~English
    /// \brief Function that is invoked each time the widget or
    ///  its parent receives a repaint signal.
    /// \~Spanish
    /// \brief Funcion que es invocada automaticamente cada vez que el widget
    ///  o su padre recive una senal de repintar.
    ///
    void paintEvent(QPaintEvent *event);
    QTimer *myTimer;

};

#endif // FROG_H

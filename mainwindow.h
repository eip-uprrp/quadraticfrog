 #ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QTimer>
#include "frog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Default constructor.
    /// \~Spanish
    /// \brief Constructor por defecto.
    ///
    explicit MainWindow(QWidget *parent = 0);

    /// \fn MainWindow::~MainWindow()
    /// \~English
    /// \brief Default destructor
    /// \~Spanish
    /// \brief Destructor por defecto
    ///
    ~MainWindow();

    /// \fn void MainWindow::quadraticFormulaCheck()
    /// \~English
    /// \brief Function that uses an array of values to determine
    ///  if the equation written by the student is correct.
    /// \~Spanish
    /// \brief Funcion que utiliza un arreglo de valores para
    ///  determinar si la ecuacion que escribio el estudiante
    ///  esta correcta.
    ///
    void quadraticFormulaCheck();

private slots:
    /// \fn void on_RunPushButton_clicked()
    /// \~English
    /// \brief Function that is invoked when the jump button is pressed,
    ///  frog jumps to calculated location.
    /// \~Spanish
    /// \brief Funcion que se invoca cuando el boton de "jump" es presionado,
    ///  el sapo brinca a la localizacion calculada.
    ///
    void on_RunPushButton_clicked();

    /// \fn void on_retryButton_clicked()
    /// \~English
    /// \brief Function that is invoked when the reset button is pressed,
    ///  returns the window to its original state.
    /// \~Spanish
    /// \brief Funcion que se invoca cuando el boton de "reset" es presionado,
    ///  devuelve la ventana a su estado original.
    ///
    void on_retryButton_clicked();

    /// \fn MainWindow::on_alineEdit_textChanged(QString &arg1)
    /// \~English
    /// \brief Function that is invoked when the text is changed in the
    ///  alineEdit, enables jump buttton if the other line edits aren't empty.
    /// \~Spanish
    /// \brief Funcion que se invoca cuando el texto dentro de alineEdit
    ///  cambia, permite que el button de "jump" sea presionado si los otros
    ///  "line edits" no estan vacios.
    ///
    void on_alineEdit_textChanged();

    /// \fn MainWindow::on_blineEdit_textChanged(QString &arg1)
    /// \~English
    /// \brief Function that is invoked when the text is changed in the
    ///  alineEdit, enables jump buttton if the other line edits aren't empty.
    /// \~Spanish
    /// \brief Funcion que se invoca cuando el texto dentro de alineEdit
    ///  cambia, permite que el button de "jump" sea presionado si los otros
    ///  "line edits" no estan vacios.
    ///
    void on_blineEdit_textChanged();

    /// \fn MainWindow::on_clineEdit_textChanged(QString &arg1)
    /// \~English
    /// \brief Function that is invoked when the text is changed in the
    ///  alineEdit, enables jump buttton if the other line edits aren't empty.
    /// \~Spanish
    /// \brief Funcion que se invoca cuando el texto dentro de alineEdit
    ///  cambia, permite que el button de "jump" sea presionado si los otros
    ///  "line edits" no estan vacios.
    ///
    void on_clineEdit_textChanged();

private:
    Ui::MainWindow *ui;
    QGraphicsScene*scene;
    QTimer *globalTimer;
    frog *frogger;
};

#endif // MAINWINDOW_H

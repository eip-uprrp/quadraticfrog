#include "frog.h"

/// \fn frog::QuadraticPlus(int a, int b, int c)
/// \~English
/// \brief Function that returns the result of the negative quadratic formula.
/// \param a: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param b: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param c: value used to complete the quadratic formula ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \~Spanish
/// \brief Funcion que devuelve el resultado de la formula cuadratica negativa.
/// \param a: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param b: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param c: valor utilizado para completar la formula quadratica ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a)
//
float frog::QuadraticPlus(int a, int b, int c){
    float result;
    result = ((-b) + sqrt((pow(b,2)) - 4*a*c))/(2*a);
    return result;
}

/// \fn frog::QuadraticMinus(int a, int b, int c)
/// \~English
/// \brief Function that returns the result of the negative quadratic formula.
/// \param a: value used to complete the quadratic formula ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param b: value used to complete the quadratic formula ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param c: value used to complete the quadratic formula ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \~Spanish
/// \brief Funcion que devuelve el resultado de la formula cuadratica negativa.
/// \param a: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param b: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
/// \param c: valor utilizado para completar la formula quadratica ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a)
//
float frog::QuadraticMinus(int a, int b, int c){
    float result;
    result = ((-b) - sqrt((pow(b,2)) - 4*a*c))/(2*a);
    return result;
}

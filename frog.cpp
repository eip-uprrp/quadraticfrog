///
/// Modifications:
///     - 2015-06-05 [Ian Davila] - Changed Frogger's location
///
#include "frog.h"
#include <QDebug>
#include <cstdlib>
#include <ctime>
#include <cassert>
#include <QMessageBox>
#include "mainwindow.h"
#include "ui_mainwindow.h"

/// \fn frog::frog(QWidget *parent)
/// \~English
/// \brief Default constructor.
/// * Ysapo: Y coordinate of the frog, set at 190 so it sits on top of the leaf
/// * Xsapo: X coordinate of the frog, a random number from 0 to -30
/// * xnenu, xnenu2, xnenu3: X coordinates of the leafs
/// * textBool: Boolean for the labels on the leafs
/// * temp, temp2: X coordinate of the background
/// \~Spanish
/// \ brief Constructor por defecto.
/// * Ysapo: Coordenada Y del sapo, inicializado a 190 para que aparesca encima del nenufar
/// * Xsapo: Coordenada X del sapo, un numero aleatorio de 0 a -30
/// * xnenu, xnenu2, xnenu3: coordenadas x de las hojas
/// * textBool: Booleana para las etiquetas de los nenufares
/// * temp, temp2: Coordenada X del fondo
///
frog::frog(QWidget *parent) :
    QWidget(parent)
{
    myTimer= new QTimer(this);
    flag = xFlag = backFlag = moved = textBool = false;
    Ysapo = 190;
    A = B = C = 0;
    temp = 580;
    temp2 = 0;
    xnenu = -(rand()%30);
    initialX = Xsapo = xnenu;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;
}

/// \fn void frog::updatenenu()
/// \~English
/// \brief Function that updates the position of the frog and the leafs
///  when the frog reaches the end of the screen when it's sliding back.
/// \~Spanish
/// \brief Funcion que actualiza la posicion del sapo y los nenufares
///  cuando el sapo llega al final de la pantalla cuando se desliza hacia atras.
///
void frog::updatenenu(){
    xnenu = -(rand() % 30);
    initialX = Xsapo = xnenu;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;
}

/// \fn void frog::reset()
/// \~English
/// \brief Function that is invoked when the retry button is clicked, resets
///  the frog and the leafs to a random starting position.
/// \~Spanish
/// \brief Funcion que es invocada cuando el boton de "retry" es presionado,
///  restablece el sapo y los nenufares a una posicion de partida aleatoria.
///
void frog::reset(){
    flag = xFlag = backFlag = moved = textBool = false;
    Ysapo = 190;
    A = B = C = 0;
    temp = 580;
    temp2 = 0;
    repaint();
    myTimer->stop();
    delete myTimer;
    myTimer = new QTimer;
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    xnenu = -(rand() %30);
    initialX = Xsapo = xnenu;
    xnenu2 = (rand() % 100) + 100;
    xnenu3 = xnenu2 + (rand() % 100) + 200;

}

/// \fn void frog::paintEvent(QPaintEvent *event)
/// \~English
/// \brief Function that is invoked each time the widget or
///  its parent receives a repaint signal.
/// \~Spanish
/// \brief Funcion que es invocada automaticamente cada vez que el widget
///  o su padre recive una senal de repintar.
///
void frog::paintEvent(QPaintEvent *){
    QPainter p(this);
    QString frogger = ":/images/Frogger1.png";
    QString frogger2 = ":/images/Frogger2.png";
    QString frogger3 = ":/images/Frogger3.png";
    QString leaf = ":/images/Nenufar1.png";
    QString back = ":/images/background.jpg";

    //Conditions that allow a margin of error (not landing in the exact x calculated)
    if(backFlag == true || (Xsapo <= (xnenu3 + 55) && Xsapo >= (xnenu3 - 55) && Ysapo == 190)
            || (Xsapo > xnenu && Ysapo == 190 && Xsapo < (xnenu2-55))
            || (Xsapo > xnenu2 && Ysapo == 190 && Xsapo < (xnenu3-55))
            || (Xsapo > xnenu3 && Ysapo == 190)){

        backFlag = true;
        myTimer->stop();
        delete myTimer;
        myTimer = new QTimer;
        QThread::msleep(5);
        connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

        // Draw background
        p.drawPixmap(temp2,-5,600,400,QPixmap(back));
        p.drawPixmap(temp,-5,600,400,QPixmap(back));

        // Draw leaves
        p.drawPixmap(xnenu,230,125,60,QPixmap(leaf));
        p.drawPixmap(xnenu2,230,125,60,QPixmap(leaf));
        p.drawPixmap(xnenu3,230,125,60,QPixmap(leaf));

        // Draw text labels
        p.drawText(xnenu2+45,310,"x1 = " + QString::number(xnenu2));
        p.drawText(xnenu3+45,310,"x2 = " + QString::number(xnenu3));

        // Moves everything on the screen back on the x-axis
        Xsapo=Xsapo-1;
        xnenu=xnenu-1;
        xnenu2=xnenu2-1;
        xnenu3=xnenu3-1;
        temp= temp - 1;
        temp2 = temp2 -1;

        repaint();

        // When the frog reaches the end of the screen reset everything to a start position
        if(temp == 0 || xnenu3 == -10 || Xsapo == 0){
            backFlag = false;
            temp = 580;
            temp2 = 0;
            moved = true;
            textBool = false;
            updatenenu();
        }

    }else{
          QFont font = p.font();
          font.setBold(true);
          p.setFont(font);

          // Draw background
          p.drawPixmap(temp2,-5,600,400,QPixmap(back));
          p.drawPixmap(temp,-5,600,400,QPixmap(back));

          // Draw leaves
          p.drawPixmap(xnenu,230,125,60,QPixmap(leaf));
          p.drawPixmap(xnenu2,230,125,60,QPixmap(leaf));
          p.drawPixmap(xnenu3,230,125,60,QPixmap(leaf));

          //Cambiada la condicion para que sea exacta la caida en el nenufar // Funciona con esta condición
          if(Xsapo < (xnenu2 + 55) && Ysapo == 190 && Xsapo > (xnenu2-55)){
               textBool = true;
          }

          // When the frog lands on the second leaf, change labels to next leaf
          if(Ysapo == 190 && Xsapo == xnenu2){
            textBool = true;
          }

          if(textBool){
              // Redraw the text labels
              p.drawText(xnenu2+35,310,"x1 = " + QString::number(xnenu2)); // de 290 a 310
              p.drawText(xnenu3+35,310,"x2 = " + QString::number(xnenu3));
              p.drawPixmap(xnenu,230,125,60,QPixmap(leaf)); // 250 a 230
          }else{
              p.drawText(xnenu2+35,310,"x2 = " + QString::number(xnenu2)); // de 290 a 310
          }
    }

    //Cambiada la condicion para que sea exacta la caida en el nenufar
    if((Xsapo > (xnenu + 55) && Ysapo == 190 && Xsapo < (xnenu2-55))
        || (Xsapo > (xnenu2 + 55) && Ysapo == 190 && Xsapo < (xnenu3-55))
        || (Xsapo > (xnenu3 + 55) && Ysapo == 190)){

        p.drawPixmap(Xsapo, Ysapo + 30, 100, 80, QPixmap(frogger3));

    }else if(Ysapo < 190){
       p.drawPixmap(Xsapo,Ysapo,100,100,QPixmap(frogger2));

    }else if(Ysapo == 190){
        qDebug() << "xsapo: " << Xsapo << "xnenu2: " << xnenu2;
        p.drawPixmap(Xsapo + 15,Ysapo - 20 ,100,100,QPixmap(frogger)); //+15 a la Xsapo, -20 a Ysapo
        if(!textBool){
            p.drawText(xnenu+45,310,"x1 = " + QString::number(xnenu+10));
        }
    }
}

/// \fun frog::~frog()
/// \~English
/// \brief Default destructor.
/// \~Spanish
/// \brief Destructor por defecto.
///
frog::~frog(){
    delete myTimer;
}

/// \fun void frog::mySlot()
/// \~English
/// \brief Function connected to a timer, used to simulate the
///  frog's jump.
/// \~Spanish
/// \brief Funcion conectada a un reloj, utilizada para simular
///  el brinco del sapo
///
void frog::mySlot(){
    float interX1 = 0;
    float interX2 = 0;
    float temp = 0;
    float y = 0;
    float xtemp = 0;
    float xEnd = 0;

    interX1 = QuadraticMinus(A,B,C);
    interX2 = QuadraticPlus(A,B,C);

    if(interX1 > interX2){
        temp = interX1;
        interX1 = interX2;
        interX2 = temp;
    }
    if(moved){
        moved = false;
        initialX = Xsapo;
    }

    xEnd = interX2 + initialX;
        y = 1;
        xtemp = 1;
        if(!xFlag){
            Ysapo = Ysapo - y;
            Xsapo = Xsapo + xtemp;
        }
        else if(xFlag){
            Ysapo = Ysapo + y;
            Xsapo = Xsapo + xtemp;
        }

        if(int(Xsapo) == int(xEnd/2)){
            xFlag = true;
        }

    repaint();
    if((int(Xsapo) == int(xEnd) && xFlag == true) || Ysapo == 190){
        Ysapo = 190;
        repaint();

        myTimer->stop();
        delete myTimer;
        myTimer = new QTimer;
        connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

        initialX = Xsapo;
        xFlag = false;
    }

}

/// \fn void frog::run(int aa, int bb, int cc)
/// \~English
/// \brief Function that is invoked when the jump button is
///  pressed, makes the frog jump to the coordinates calculated
///  with the a, b, c values provided by the user.
/// \param aa value provided by user to complete the quadratic equation ax^2 + bx + c
/// \param bb value provided by user to complete the quadratic equation ax^2 + bx + c
/// \param cc value provided by user to complete the quadratic equation ax^2 + bx + c
/// \~Spanish
/// \brief Funcion que es invocada cuando el boton de brincar es
///  presionado, hace que el sapo salte a las coordenadas calculadas
///  con los valores de a, b, c proporcionados por el usuario.
/// \param aa valor proporcionado por el usuario utilizado para completar la ecuacion cuadratica ax^2 + bx + c
/// \param bb valor proporcionado por el usuario utilizado para completar la ecuacion cuadratica ax^2 + bx + c
/// \param cc valor proporcionado por el usuario utilizado para completar la ecuacion cuadratica ax^2 + bx + c
///
void frog::run(int aa, int bb, int cc){

    A = aa;
    B = bb;
    C = cc;

    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));
    myTimer->start(25);
}
